Bitte beachte: Dies ist eine unoffizielle Übersetzung des [Englischen Originaldokuments](../PrivacyPolicy.md). Bitte lies auch die weiteren Hinweise im Überordner dieses Repositories.

## Kurz gesagt: Du hast die volle Kontrolle und Hoheit über deine Daten!

Wir haben einen Minimalsammlungs-Leitfaden. Abgesehen von notwendigen Daten, um den Service am LAufen zu halten, sammeln wir keine Nutzer- und Trackingdaten.

### Nutzerbeiträge

Du hast die volle Kontrolle, Daten die zu hinzufügst (z. B. Code und Inhalt, Kommentare, Repositories, Accountdaten und Einstellungen) zu erstellen, zu erweitern und zu ändern. Falls Daten auf der Plattform nicht durch Nutzer:innen geändert werden können, ist dies als technischer Fehler zu betrachten und muss schnellstens behoben werden. Bitte melde solche Fälle und andere Fehler an den [Codeberg.org Community Issue Tracker](/Codeberg/Community/issues).

### Backupdateien

Nachdem du Daten in deinem Acount gelöscht hast, können rotierende Offline-Backups für bis zu 30 Tage nach DSGVO-Richtlinien weiter existieren.

### IP-Speicherung

Serverlogs können IP-Adressen und Useragenten-Informationen von den verbundenen Computern enthalten. Diese Logs werden automatisch nach spätestens sieben Tagen gelöscht.

### Codeberg e.V. Mitgliedschaftsdaten

Wir sind gesetzlich dazu verpflichtet, aktuelle Aufzeichnungen unserer Mitgliederdaten zu pflegen. Dies betrifft die Daten, die Du auf join.codeberg.org in das Registrierungsformular eingibst, solltest du dem Codeberg e. V. beitreten wollen, also Name, Adresse, Kontaktmöglichkeit und eine Bankverbindung. Diese Aufzeichnungen sind mit Public-Key-Verschlüssung direkt nach dem Absenden des Formulars verschlüsselt. Weiterhin fügt das Beitrittsformular noch den Zeitpunkt und die IP-Adresse an die Aufzeichnung an, um Missbrauch zu erkennen und zu bekämpfen.

### Aufzeichnungen der Testinstanz und der Bekanntmachungs-Mailingliste

Die Bekanntmachungs-Mailingliste war für einmaligen Gebrauch bestimmt und wurde nach dem Bekanntwerden der Eröffnung von Codeberg.org gelöscht. Accounts der Testinstanz wurden auf Codeberg.org migriert, wo dies möglich war. Diese Accounts und ihre verbundenen Daten können über die Funktion "Account löschen" in den persönlichen Nutzereinstellungen gelöscht werden. Alle unbenutzten Daten der Testserver wurden beim Start vernichtet.
